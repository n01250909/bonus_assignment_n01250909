﻿<%@ Page Language="C#" Inherits="Bonus_Assignment_n01250909.Default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Bonus Assignment</title>
</head>
<body>
    <form id="form" runat="server">

<!-- Question 1 -->
    <h3>Cartesian Smartesian</h3>
<!-- Textbox for x -->
    <asp:TextBox runat="server" id="x_axis" placeholder="Enter a x-axis"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" ID="x_validator" Display = "Dynamic" ControlToValidate="x_axis" ErrorMessage="Please Enter a Number"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator runat="server" ID="x_RegularExpressionValidator" Display = "Dynamic" ControlToValidate = "x_axis" ValidationExpression = "^-?[0-9]\d*(\.\d+)?$" ErrorMessage="Please Enter a Valid Number!"></asp:RegularExpressionValidator></br></br>
<!-- Textbox for x -->
    <asp:TextBox runat="server" id="y_axis" placeholder="Enter a y-axis"></asp:TextBox>
<!-- Validations --> 
    <asp:RequiredFieldValidator runat="server" ID="y_validator" Display = "Dynamic" ControlToValidate="y_axis" ErrorMessage="Please Enter a Number"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator runat="server" ID="y_RegularExpressionValidator" Display = "Dynamic" ControlToValidate = "y_axis" ValidationExpression = "^-?[0-9]\d*(\.\d+)?$" ErrorMessage="Please Enter a Valid Number!"></asp:RegularExpressionValidator></br></br>
<!-- Button -->    
    <asp:Button runat="server" id="button_1" Text="Check" OnClick="Check1Clicked" /> 
    <div runat="server" id="result_q1"></div> 
         
         
<!-- Question 2 -->
    <h3>Divisible Sizzable</h3>
<!-- Textbox for divisible -->
    <asp:TextBox runat="server" id="divisible" placeholder="Enter a Number"></asp:TextBox>
<!-- Validations -->    
    <asp:RequiredFieldValidator runat="server" ID="divisible_validator" Display = "Dynamic" ErrorMessage="Please enter a number" ControlToValidate="divisible"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator  runat="server" ID="divisible_RegularExpressionValidator" Display = "Dynamic" ControlToValidate = "divisible" ValidationExpression = "^[1-9]\d*(\.\d+)?$" ErrorMessage="Please Enter a Valid Number!"></asp:RegularExpressionValidator></br></br>
<!-- Button -->    
    <asp:Button runat="server" id="button_2" Text="Check" OnClick="Check2Clicked" />    
    <div runat="server" id="result_q2"></div> 
            
<!-- Question 3 -->
    <h3>String Bling</h3>
<!-- Textbox for palindromes -->
    <asp:TextBox runat="server" id="palindromes_input" placeholder="Enter a Word"></asp:TextBox>
<!-- Validations -->    
    <asp:RequiredFieldValidator runat="server" ID="palindromes_validator" Display = "Dynamic" ErrorMessage="Please enter a word" ControlToValidate="palindromes_input"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator runat="server" ID="palindromes_RegularExpressionValidator" Display = "Dynamic" ControlToValidate = "palindromes_input" ValidationExpression = "^[a-zA-Z]+$" ErrorMessage="Please Enter a Valid Word!"></asp:RegularExpressionValidator></br></br>
<!-- Button -->
     <asp:Button runat="server" id="button_3" Text="Check" OnClick="Check3Clicked" />    
    <div runat="server" id="result_q3"></div> 
    </form>
</body>
</html>