﻿using System;
using System.Web;
using System.Web.UI;

namespace Bonus_Assignment_n01250909
{

public partial class Default : System.Web.UI.Page
    {
        //QUESTION 1//
        public void Check1Clicked(object sender, EventArgs args){
            string x = x_axis.Text.ToString();
            string y = y_axis.Text.ToString();
            string rel = "";
            string negative = "-";
            string q1 = "The value is falls on Quadrant I";
            string q2 = "The value is falls on Quadrant II";
            string q3 = "The value is falls on Quadrant III";
            string q4 = "The value is falls on Quadrant IV";
            string substx = x.Substring(0, 1);
            string substy = y.Substring(0, 1);
            if (negative != substx && negative != substy){ rel = q1;}
            else if (negative == substx && negative != substy){ rel = q2;}
            else if (negative == substx && negative == substy){ rel = q3;}
            else if (negative != substx && negative == substy){ rel = q4;}
            result_q1.InnerHtml = rel;
        }

        //QUESTION 2//
        public static bool IsPrime(int number)
        {
            if (number == 2){return true;}
            if (number <= 1){return false;}
            if (number % 2 == 0){ return false;}

            var boundary = (int)Math.Floor(Math.Sqrt(number));
            for (int i = 3; i <= boundary; i += 2){
            if (number % i == 0){return false;}
            } return true;}
        
        public void Check2Clicked(object sender, EventArgs args){
            string inp = divisible.Text.ToString();
            int num_inp = int.Parse(inp);
            string msg = "";
            if (IsPrime(num_inp)){ msg = "The number you put is a prime number";}
            else { msg = "The number you put is non-prime number";}
            result_q2.InnerHtml = msg;
        }

        //QUESTION 3//
        public void Check3Clicked(object sender, EventArgs args){
        //(transform the string to all lowercase letters, than I remove the string’s spaces, and run a loop as many times as the length of the computed string).
            string inp = palindromes_input.Text.ToString().ToLower().Replace(" ", string.Empty);
            char[] charArray = inp.ToCharArray();
            Array.Reverse(charArray);
            string ns = new string(charArray);
            string result_q2 = "";
            if (inp == ns){ result_q2 = "The word you put is palindrome";
            } else{ result_q2 = "T you put is not a palindrome";}
            result_q3.InnerHtml = result_q2;
        }
    }
}